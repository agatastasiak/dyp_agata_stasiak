export interface IUser {
    value: number;
    role: string;
    timestamp: string;
    type: string;
}
