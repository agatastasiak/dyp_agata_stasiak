import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { IUser } from 'src/app/users/IUser.model';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: IUser[];
  usersSub: Subscription;

  isLoading = false;
  error = false;

  data: any[];
  view: any[] = [1400, 700];

  option: string;
  paramsSub: Subscription;

  constructor(
    private userService: UserService, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.paramsSub = this.route.queryParams.subscribe(params => {
      if (params.option) {
        this.getData(params.option);
      }
    })
  }

  getData(type: string) {
    this.isLoading = true;
    this.error = false;
    this.usersSub = this.userService.findByType(type)
      .subscribe(promiseResult => {
        this.users = promiseResult;
        this.data = this.userService.parseData(type, promiseResult);
        this.isLoading = false;
        this.error = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
        this.error = true;
      });
  }

  ngOnDestroy() {
    this.usersSub.unsubscribe();
    this.paramsSub.unsubscribe();
  }
}
