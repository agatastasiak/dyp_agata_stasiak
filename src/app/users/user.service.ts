import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { HttpClient, HttpParams } from '../../../node_modules/@angular/common/http';
import { IUser } from './IUser.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  findByType(type: string): Observable<IUser[]> {
    return this.http.get<IUser[]>('http://localhost:8080/userReport/byTypeParam', {
      params: new HttpParams().set('type', type)
    });
  }

  getLegendForQuery(type: string): string[] {
    switch (type) {
      case 'Frequent Users':
        return ['NAT', 'Admin', 'Duel', 'EIC', 'UD_', 'VBC', 'RT_'];
      case 'Unique Users':
        return ['NAT', 'Admin', 'Duel', 'EIC', 'UD_', 'VBC', 'RT_'];
      case 'Total Users':
        return ['NAT', 'Admin', 'Duel', 'EIC', 'UD_', 'VBC', 'RT_'];
    }
  }

  parseData(type: string, input: any[]): any[] {
    const legend = this.getLegendForQuery(type);
    const table = [];
    for (let j = 0; j < legend.length; ++j) {
      const series = [];
      for (let i = 0; i < input.length; ++i) {
        if (input[i].role === legend[j]) {
          series.push({ name: input[i].timestamp, value: input[i].value });
        }
      }
      table.push({ name: legend[j], series: series });
    }
    return table;
  }
}
