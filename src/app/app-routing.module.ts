import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { UsersComponent } from './users/users.component';
import { MaintenancePlansComponent } from './maintenance-plans/maintenance-plans.component';
import { QuotationsComponent } from './quotations/quotations.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  {path: '', redirectTo: '/users', pathMatch: 'full'},
  {path: 'users', component: UsersComponent},
  {path: 'maintenancePlans', component: MaintenancePlansComponent},
  {path: 'quotations', component: QuotationsComponent},
  {path: 'contact', component: ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
