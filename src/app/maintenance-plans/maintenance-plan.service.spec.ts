import { TestBed, inject } from '@angular/core/testing';
import { MaintenancePlanService } from './maintenance-plan.service';



describe('MaintenancePlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MaintenancePlanService]
    });
  });

  it('should be created', inject([MaintenancePlanService], (service: MaintenancePlanService) => {
    expect(service).toBeTruthy();
  }));
});
