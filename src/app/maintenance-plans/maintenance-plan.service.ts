import { Injectable} from '@angular/core';
import { HttpClient, HttpParams } from '../../../node_modules/@angular/common/http';
import { Observable } from 'rxjs';
import { IMP } from './IMP.model';


@Injectable({
  providedIn: 'root'
})
export class MaintenancePlanService {

  constructor(private http: HttpClient) { }

  findByType(type: string): Observable<IMP[]> {
    return this.http.get<IMP[]>('http://localhost:8080/MaintPlanQuotation/byTypeParam', {
      params: new HttpParams().set('type', type)
    });
  }

  getLegendForQuery(type: string): string[] {
    return ['PREVOST', 'VTC', 'EICHER', 'VBC', 'RTC', 'UD', 'VCE'];
  }

  parseData(type: string, input: any[]): any[] {
    const legend = this.getLegendForQuery(type);
    const multi = [];
    for (let j = 0; j < legend.length; ++j) {
      const series = [];
      for (let i = 0; i < input.length; ++i) {
        if (input[i].maintenanceresponsible === legend[j]) {
          series.push({ name: input[i].timestamp, value: input[i].value });
        }
      }
      multi.push({ name: legend[j], series: series });
    }
    return multi;
  }
}
