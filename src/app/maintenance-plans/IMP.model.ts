export interface IMP {
    value: number;
    maintenanceresponsible: string;
    timestamp: string;
    type: string;
}
