import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MaintenancePlanService } from './maintenance-plan.service';
import { ActivatedRoute } from '@angular/router';
import { IMP } from 'src/app/maintenance-plans/IMP.model';

@Component({
  selector: 'app-maintenance-plans',
  templateUrl: './maintenance-plans.component.html',
  styleUrls: ['./maintenance-plans.component.css']
})
export class MaintenancePlansComponent implements OnInit {

  mps: IMP[];
  mpSub: Subscription;

  isLoading = false;
  error = false;

  data: any[];
  view: any[] = [700, 400];

  option: string;
  paramsSub: Subscription;

  constructor(
    private mpService: MaintenancePlanService, private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.paramsSub = this.route.queryParams.subscribe(params => {
      if (params.option) {
        this.getData(params.option);
      }
    })
  }

  getData(type: string) {
    this.isLoading = true;
    this.error = false;
    this.mpSub = this.mpService.findByType(type)
      .subscribe(promiseResult => {
        this.mps = promiseResult;
        this.data = this.mpService.parseData(type, promiseResult);
        this.isLoading = false;
        this.error = false;
      }, error => {
        console.log(error);
        this.isLoading = false;
        this.error = true;
      });
  }

  ngOnDestroy() {
    this.mpSub.unsubscribe();
    this.paramsSub.unsubscribe();
  }

}
